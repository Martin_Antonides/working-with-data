import classs from './styles.scss';
import $ from "jquery";

let dataManager;
let offset = 0;

$(document).on("click",".next:not(.clicked)", function(){
    offset = offset + 10;
    dataManager.renderData('main');
});

$(document).on("click",".previous:not(.clicked)", function(){
    offset = offset - 10;
    dataManager.renderData('main');
});

$(document).on("click",".load-data:not('.clicked')", function(){
    offset = 0;
    let el = $(this);
    el.removeClass('clickable');
    el.addClass('clicked');
    dataManager = new DataManager('GET', "application/json", "https://jsonplaceholder.typicode.com/posts");
    dataManager.fetchRequest().then(function(response) {
        dataManager.data = response
        dataManager.renderData('main');
        $('.pagination-controls').show();
        el.removeClass('clicked');
        el.addClass('clickable');
      }, function(error) {
        console.error("Failed!", error);
        el.removeClass('clicked');
        el.addClass('clickable');
    })
});

function DataManager(fetchType, contentType, url){
    this.fetchType = fetchType;
    this.contentType = contentType;
    this.url = url;
    this.data = null;
}

DataManager.prototype.showObject = function(divId, {id = 0, userId = 0, title = "No Title", body = "No Body"}){
    return (
        `<div class="card-wrapper">
            <span class="card-id">${id}</span>
            <div class="card-item card-title">${title}</div>
            <div class="card-item card-meta-data">
                <span class="userId">User ID: ${this.getUser(userId)}</span>
            </div>
            <div class="card-item card-body">${body}</div>
        </div>`
    )
}

DataManager.prototype.renderData = function(divId){
    let partial = ``;

    let data = this.data.slice(offset, offset + 10);

    data.forEach(obj => {
        partial = partial + (this.showObject(divId, obj));
    });

    $('#'+divId).html(partial);

    if(offset === 0){
        $('.previous').addClass('clicked');
        $('.next').removeClass('clicked');
    }

    if(offset === 10){
        $('.previous').removeClass('clicked');
    }

    if(offset === 80){
        $('.next').removeClass('clicked');
    }

    if(offset === 90){
        $('.next').addClass('clicked');
    }
}

DataManager.prototype.getUser = function(userId){
    return ({
        1 : "Bob", 
        2 : "Michelle", 
        3 : "Jason", 
        4 : "Terry", 
        5 : "Keenan", 
        6 : "Daizy", 
        7 : "Gwen", 
        8 : "Jamie", 
        9 : "Barbera", 
        10 : "Gabby"
    })[userId];
}

DataManager.prototype.fetchRequest = function(){
    let request = ({
        "GET" : new Request(this.url, {
	        method: 'GET', 
	        headers: new Headers({
		       'Content-Type': this.contentType
	        })
        }),
        "POST" : new Request(this.url, {
            method: 'POST', 
            headers: new Headers({
               'Content-Type': this.contentType,
            }),
            data: this.data
        }),
        "PUT" : new Request(this.url, {
            method: 'PUT', 
            headers: new Headers({
               'Content-Type': this.contentType,
            }),
            data: this.data
        })
    })[this.fetchType];

    return fetch(request).then(function(response) {
        let data = response.json()
        return data;
    }).catch(function(err) {
        console.log('Error with fetch method')
        return err;
    });
}

function Person(first, last, age) {
    this.firstName = first;
    this.lastName = last;
    this.age = age;
}

function Teacher(first, last, age, subject) {
    Person.call(this, first, last, age);

    this.subject = subject;
}

Teacher.prototype = Object.create(Person.prototype);

Person.prototype.name = function() {
    return this.firstName + " " + this.lastName;
};

Teacher.prototype.subjectType = function() {
    return this.name() + " is teaching " + this.subject+ ".";
};

$(document).ready(function(){   
    let newTeacher = new Teacher('John', 'Doe', '29', 'Blue');

    console.log(newTeacher.firstName);
    console.log(newTeacher.lastName);
    console.log(newTeacher.name());
    console.log(newTeacher.subjectType());
});